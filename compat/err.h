/*
 * err.h compatibility shim
 */

#ifndef MSYS2_PKGS_ERR_H
#define MSYS2_PKGS_ERR_H

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NORETURN __attribute__((noreturn))

NORETURN static inline void err(int eval, const char *fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    if (fmt != NULL) {
        vfprintf(stderr, fmt, ap);
        fprintf(stderr, ": ");
    }
    va_end(ap);
    fprintf(stderr, "%s\n", strerror(errno));
    exit(eval);
}

NORETURN static inline void errx(int eval, const char *fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    if (fmt != NULL)
        vfprintf(stderr, fmt, ap);
    va_end(ap);
    fprintf(stderr, "\n");
    exit(eval);
}

static inline void warn(const char *fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    if (fmt != NULL) {
        vfprintf(stderr, fmt, ap);
        fprintf(stderr, ": ");
    }
    va_end(ap);
    fprintf(stderr, "%s\n", strerror(errno));
}

static inline void vwarnx(const char *fmt, va_list args) {
    if (fmt != NULL)
        vfprintf(stderr, fmt, args);
    fprintf(stderr, "\n");
}

static inline void warnx(const char *fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    vwarnx(fmt, ap);
    va_end(ap);
}

#endif // MSYS2_PKGS_ERR_H
