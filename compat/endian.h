/*
 * endian.h compatibility shim
 */

#ifndef MSYS2_PKGS_ENDIAN_H
#define MSYS2_PKGS_ENDIAN_H

#include <winsock2.h>

#define LITTLE_ENDIAN 1234
#define BIG_ENDIAN    4321
#define PDP_ENDIAN    3412

#define BYTE_ORDER LITTLE_ENDIAN

#define htobe16(x) htons(x)
#define htole16(x) (x)
#define be16toh(x) ntohs(x)
#define le16toh(x) (x)

#define htobe32(x) ntohl(x)
#define htole32(x) (x)
#define be32toh(x) ntohl(x)
#define le32toh(x) (x)

#define htobe64(x) htonll(x)
#define htole64(x) (x)
#define be64toh(x) ntohll(x)
#define le64toh(x) (x)

#endif // MSYS2_PKGS_ENDIAN_H
